<?php

/**
 * @file
 *   Allows anonymous users to retain their identity and ownership of posts.
 * @todo
 *   - Figure out how to react when a different user edits content created by an
 *     anonymous user.
 *   - Link anonymous user names to their profile page.
 *   - Add a way for admin to block anonymous user profile pages.
 *   - Add a setting to allow mapping "name" to a field other than "username"
 *     on the registration form.
 *   - Fix silent updating in sup_user().
 *   - Add Views integration.
 *   - Support Gravatar (see sup_user(), theme_sup_user_view(), _sup_save()).
 *   - Support Userpoints.
 *   - Support User Badges.
 *   - Support Facebook-style Statuses suite.
 */

/**
 * Implementation of hook_menu().
 */
function sup_menu() {
  $items = array();
  $items['user/anon/%sup_user'] = array(
    'title' => 'My account', 
    'title callback' => 'sup_user_page_title', 
    'title arguments' => array(1), 
    'page callback' => 'sup_user_view', 
    'page arguments' => array(1), 
    'access callback' => 'sup_user_view_access', 
    'access arguments' => array(1),
  );
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function sup_perm() {
  return array('access anonymous user profiles');
}

/**
 * Implementation of hook_comment().
 */
function sup_comment($values, $op) {
  if ($op == 'delete') {
    $comment = (object) $values;
    db_query("DELETE FROM {sup_content} WHERE type = 'comment' && content_id = %d", $comment->cid);
  }
  //Anonymous users can't edit or update comments.
  elseif ($op == 'insert' && $GLOBALS['user']->uid == 0) {
    _sup_save($values['name'], $values['mail'], $values['cid'], 'comment');
  }
}

/**
 * Implementation of hook_form_alter().
 */
function sup_form_alter(&$form, &$form_state, $form_id, $node = NULL) {
  if ($GLOBALS['user']->uid != 0) {
    return;
  }
  foreach (node_get_types() as $type => $definition) {
    if ($form_id == $type .'_node_form') {
      $account = db_fetch_object(db_query("SELECT sid, name, mail FROM {sup_users} WHERE sid = %d", session_api_get_sid()));
      $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Your name'),
        '#maxlength' => 60,
        '#size' => 30,
        '#default_value' => $account->name ? $account->name : variable_get('anonymous', t('Anonymous')),
        '#weight' => -25,
      );
      $form['mail'] = array(
        '#type' => 'textfield',
        '#title' => t('E-mail'),
        '#maxlength' => 64,
        '#size' => 30,
        '#default_value' => $account->mail,
        '#description' => t('The content of this field is kept private and will not be shown publicly.'),
        '#weight' => -20,
      );
    }
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function sup_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op == 'delete') {
    db_query("DELETE FROM {sup_content} WHERE type = 'node' AND content_id = %d", $node->nid);
  }
  if ($GLOBALS['user']->uid != 0) {
    return;
  }
  if ($op == 'insert' || $op == 'update') {
    _sup_save($node->name, $node->mail, $node->nid, 'node');
  }
}

/**
 * Implementation of hook_session_api_cleanup().
 */
function sup_session_api_cleanup($outdated_sids) {
  db_query("DELETE FROM {sup_users} WHERE sid IN (". db_placeholders($outdated_sids) .")", $outdated_sids);
  db_query("DELETE FROM {sup_content} WHERE sid IN (". db_placeholders($outdated_sids) .")", $outdated_sids);
}

/**
 * Implementation of hook_user().
 */
function sup_user($op, &$edit, &$account, $category = NULL) {
  if ($GLOBALS['user']->uid != 0) {
    return;
  }
  //@todo: save $edit['sup_data'] (which will contain the Gravatar, once integrated) to the newly saved user object in the database.
  //@todo: This should really invoke hook_comment($comment, 'update'). In particular {node_comment_statistics}.last_comment_uid will not be updated here.
  db_query("UPDATE {comments} SET uid = %d WHERE uid = 0 AND cid IN (SELECT content_id FROM {sup_content} WHERE type = 'comment' AND sid = %d)", $account->uid, $edit['sup_sid']);
  //@todo: This should really invoke hook_nodeapi($node, 'update'). In particular {node_revisions}.uid will not be updated here.
  db_query("UPDATE {node} SET uid = %d WHERE uid = 0 AND nid IN (SELECT content_id FROM {sup_content} WHERE type = 'node' AND sid = %d)", $account->uid, $edit['sup_sid']);
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function sup_form_user_register_alter(&$form, &$form_state) {
  $account = db_fetch_object(db_query("SELECT sid, name, mail, data FROM {sup_users} WHERE sid = %d", session_api_get_sid()));
  $form['name']['#default_value'] = $account->name;
  $form['mail']['#default_value'] = $account->mail;
  foreach (array('sid', 'data') as $field) {
    $form['sup_'. $field] = array(
      '#type' => 'value',
      '#value' => $account->{$field},
    );
  }
}

/**
 * Loads an anonymous user's properties.
 */
function sup_user_load($sid) {
  return db_fetch_object(db_query("SELECT * FROM {sup_users} WHERE sid = %d", $sid));
}

/**
 * Implementation of hook_theme().
 */
function sup_theme($existing, $type, $theme, $path) {
  return array(
    'sup_user_view' => array(
      'arguments' => array(
        'account' => NULL,
      ),
    ),
  );
}

/**
 * Themes an anonymous user profile.
 *
 * @param $account
 *   An anonymous user's properties.
 */
function theme_sup_user_view($account) {
  $nodes = array();
  $result = db_query_range("SELECT content_id FROM {sup_content} WHERE type = 'node' AND sid = %d", $account->sid);
  while ($content = db_fetch_object($result)) {
    $node = node_load($content->content_id);
    $nodes[] = l($node->title, 'node/'. $node->nid);
  }
  $comments = array();
  $result = db_query_range("SELECT content_id FROM {sup_content} WHERE type = 'comment' AND sid = %d", $account->sid);
  while ($content = db_fetch_object($result)) {
    $comment = _comment_load($content->content_id);
    $comments[] = l($comment->subject, 'node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid));
  }
  return '<div><h3>'. t('First post') .'</h3><p>'. format_date($account->created) .'</p></div>'.
    '<div><h3>'. t('Last access') .'</h3><p>'. format_date($account->access) .'</p></div>'.
    theme('item_list', $nodes, t('Recent posts')) .
    theme('item_list', $comments, t('Recent comments'));
  //@todo: Display $account->data here too.
}

/**
 * Generates a page title for the anonymous user account page.
 *
 * @param $account
 *   An anonymous user's properties.
 */
function sup_user_page_title($account) {
  if ($account->sid == session_api_get_sid() && $GLOBALS['user']->uid == 0) {
    return t('My account');
  }
  return $account->name;
}

/**
 * The view callback for the anonymous user profile.
 *
 * @param $account
 *   An anonymous user's properties.
 */
function sup_user_view($account) {
  if ($account->sid == session_api_get_sid() && $GLOBALS['user']->uid == 0) {
    drupal_set_message(t('Hello, @name!', array('@name' => check_plain($account->name))) .' '.
      t('You are not yet registered. <a href="user/register">Register now</a> to claim your content! If you wait, you may not be able to claim it later.'));
  }
  return theme('sup_user_view', $account);
}

/**
 * Checks access to view an anonymous user's profile.
 *
 * @param $account
 *   An anonymous user's properties.
 */
function sup_user_view_access($account) {
  return $account->sid && $account->status && (
    ($account->sid == session_api_get_sid() && $GLOBALS['user']->uid == 0) ||
    user_access('access anonymous user profiles') ||
    user_access('administer users')
  );
}

/**
 * Responds to content being saved by an anonymous user.
 */
function _sup_save($name, $mail, $content_id, $type) {
  $sid = session_api_get_sid();
  $time = time();
  db_query("UPDATE {sup_users} SET name = '%s', mail = '%s', access = %d WHERE sid = %d", $name, $mail, $time, $sid);
  if (db_affected_rows() < 1) {
    $urecord = (object) array(
      'sid' => $sid,
      'name' => $name,
      'mail' => $mail,
      'created' => $time,
      'access' => $time,
      'status' => 1,
      'data' => '', //@todo: If the Gravatar module is installed, store the gravatar in data.
    );
    drupal_write_record('sup_users', $urecord);
  }
  $crecord = (object) array(
    'sid' => $sid,
    'content_id' => $content_id,
    'type' => $type,
  );
  drupal_write_record('sup_content', $crecord);
}
